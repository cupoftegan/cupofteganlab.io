---
title: "{{ replace .Name "-" " " | title }}"
description: "A great description"
publishDate: {{ .Date }}
slug: a-shorter-slug
tags:
 - bookmark
 - eco
 - freelance
 - mental health
 - recipe
 - retrospective
 - self care
 - zero waste
---
