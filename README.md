# Cup of Tegan site
This is the source code for my [Hugo](https://gohugo.io/) driven static site.

https://cupoftegan.dev

---

:eyes: Things you will find on this site:
- Blog posts [/blog](https://cupoftegan.dev/blog)
- Code snippets [/snippets](https://cupoftegan.dev/snippets)
- Labs [/labs](https://cupoftegan.dev/labs)

---

- :red_car: [Roadmap](https://gitlab.com/cupoftegan/cupoftegan.gitlab.io/boards)
- :rotating_light: [Issues](https://gitlab.com/cupoftegan/cupoftegan.gitlab.io/issues)

---

## Set up

1. Clone the repository
2. Install Go ([How to install Go on Windows](https://www.freecodecamp.org/news/setting-up-go-programming-language-on-windows-f02c8c14e2f/))
3. Install Hugo 'choco install hugo-extended -confirm'
4. Run `npm install`
5. Restart your terminal
6. Run `hugo`
